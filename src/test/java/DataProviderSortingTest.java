import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import model.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(DataProviderRunner.class)
public class DataProviderSortingTest {

    @DataProvider
    public static Object[][] nullAndEmptyCasesData() {
        return new Object[][]{
                {null},
                {new int[]{}}
        };
    }

    @DataProvider
    public static Object[][] singleCasesData() {
        return new Object[][]{
                {new int[]{5}},
                {new int[]{-73}},
                {new int[]{29}}
        };
    }

    @DataProvider
    public static Object[][] sortedArrayCasesData() {
        return new Object[][]{
                {new int[]{1, 2, 3, 4, 7}, new int[]{1, 2, 3, 4, 7}},
                {new int[]{1, 5, 7, 8, 9}, new int[]{1, 5, 7, 8, 9}},
                {new int[]{-31, -18, 12, 58, 97}, new int[]{-31, -18, 12, 58, 97}}
        };
    }

    @DataProvider
    public static Object[][] tooManyArgumentsCasesData() {
        return new Object[][]{
                {11, "Вы превысили количество аргументов", new int[]{1, 2, 3, 4, 7, 5, 7, 2, 6, 1, 22}},
        };
    }

    @DataProvider
    public static Object[][] notIntegerCasesData() {
        return new Object[][]{
                {"Некорректный ввод. Введите другое целое число", "1.5, 2.2"},
                {"Некорректный ввод. Введите другое целое число", "text"},
                {"Некорректный ввод. Введите другое целое число", "−2147483649"},
                {"Некорректный ввод. Введите другое целое число", "2147483648"}
        };
    }

    @DataProvider
    public static Object[][] otherCasesData() {
        return new Object[][]{
                {new int[]{8, 3, 9, 1, 7}, new int[]{1, 3, 7, 8, 9}},
                {new int[]{5, 2, 5, 1, 2}, new int[]{1, 2, 2, 5, 5}},
                {new int[]{-5, 12, 0, -1, 65}, new int[]{-5, -1, 0, 12, 65}}
        };
    }

    @Test(expected = IllegalArgumentException.class)
    @UseDataProvider("nullAndEmptyCasesData")
    public void testNullAndEmptyCase(int[] inputArray) {
        Sorting sorting = new Sorting();
        sorting.sort(inputArray);
    }

    @Test
    @UseDataProvider("singleCasesData")
    public void testSingleCase(int[] inputArray) {
        Sorting sorting = new Sorting();
        sorting.sort(inputArray);
    }

    @Test
    @UseDataProvider("sortedArrayCasesData")
    public void testSortedArraysCase(int[] inputArray, int[] expectedArray) {
        Sorting sorting = new Sorting();
        sorting.sort(inputArray);
        assertArrayEquals(expectedArray, inputArray);
    }

    @Test
    @UseDataProvider("tooManyArgumentsCasesData")
    public void testTooManyArguments(int expectedLength, String expectedMessage, int[] inputArray) {
        if (inputArray.length > 10) {
            assertEquals(expectedMessage, "Вы превысили количество аргументов");
        } else {
            assertEquals(expectedLength, inputArray.length);
        }
    }

    @Test(expected = NumberFormatException.class)
    @UseDataProvider("notIntegerCasesData")
    public void testNotIntegerInput(String expectedMessage, String input) {

        String[] numbersInputArray = input.split(" ");
        int[] numbersArray = new int[numbersInputArray.length];

        for (int i = 0; i < numbersInputArray.length; i++) {
            numbersArray[i] = Integer.parseInt(numbersInputArray[i]);

            assertEquals(expectedMessage, "Некорректный ввод. Введите другое целое число");
        }
    }

    @Test
    @UseDataProvider("otherCasesData")
    public void testOtherCases(int[] inputArray, int[] expectedArray) {
        Sorting sorting = new Sorting();
        sorting.sort(inputArray);
        assertArrayEquals(expectedArray, inputArray);
    }
}
