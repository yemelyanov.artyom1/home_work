package model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Scanner;

public class Sorting {

    private static final Logger LOGGER = LoggerFactory.getLogger(Sorting.class);

    public static void main(String[] args) {

        System.out.println("Enter an array of integers separated by spaces:");
        Scanner scanner = new Scanner(System.in);
        String numbersInput = scanner.nextLine();

        inputHandler(numbersInput);

    }

    /**
     * This is a JavaDoc comment for a method inputHandler. This method formats the input value from the console.
     *
     * @param numbersInput Takes a string from the console
     * @throws NumberFormatException in cases when the user inputs incorrect values:
     *                               text, floating-point numbers, and values outside the range of the int type
     */
    public static void inputHandler(String numbersInput) throws NumberFormatException {
        try {
            String[] numbersInputArray = numbersInput.split(" ");

            int[] numbersArray = new int[numbersInputArray.length];
            if (numbersInputArray.length > 10) {
                System.out.println("You have exceeded the number of arguments");
            } else {
                for (int i = 0; i < numbersInputArray.length; i++) {
                    numbersArray[i] = Integer.parseInt(numbersInputArray[i]);
                }

                Sorting sorting = new Sorting();
                sorting.sort(numbersArray);
                System.out.println(Arrays.toString(numbersArray));
            }
        } catch (NumberFormatException e) {
            System.out.println("Incorrect input. Please enter another integer");
        }
    }

    /**
     * This is a JavaDoc comment for a method sort. This method sorts the numbers in ascending order.
     *
     * @param array Accepts an array of integers
     */
    public void sort(int[] array) {

        if ((array == null) || (array.length == 0)) {
            String errorMessage = "An empty array or null was passed.";
            LOGGER.error(errorMessage);
            throw new IllegalArgumentException(errorMessage);
        }

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int maxElement = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = maxElement;
                }
            }
        }
    }
}